// See: https://www.npmjs.com/package/ftp-deploy

let FtpDeploy = require('ftp-deploy');
let ftpDeploy = new FtpDeploy();
let _ = require('lodash');

let argsList = _.drop(process.argv, 2); // remove 'node' and this file
let args = {
    username: argsList[0],
    password: argsList[1], // NB: Some special characters require ridiculous escaping on Windows. E.g. ^ becomes ^^^^. More info: https://stackoverflow.com/questions/11193691/batch-escaping-with-caret
};

console.log('Deploying...');
// console.log('Args', args);

let config = {
    user: args.username,
    password: args.password,
    host: "ftp.backofen.us",
    port: 21,
    include: ['*', '**/*'],
    localRoot: __dirname + "/../dist",
    remoteRoot: "/public_html/battle-city/card-game",
}

ftpDeploy.deploy(config, function(err) {
    if (err) {
        console.log(err);
        process.exit(1);
    }

    console.log('Deploy successful.');
    process.exit(0);
});
