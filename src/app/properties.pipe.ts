import { Pipe, PipeTransform } from '@angular/core';
import * as _ from 'lodash';

@Pipe({
  name: 'properties'
})
export class PropertiesPipe implements PipeTransform {

  transform(obj: any, args?: any): any {
    console.log('piping ', obj);
    if (!obj) return null;

    return _.map(obj, (val, index) => {
      return { index: index, val: val };
    });
  }

}
