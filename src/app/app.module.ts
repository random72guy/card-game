import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HttpClient } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { PropertiesPipe } from './properties.pipe';
import { GameHandComponent } from './components/game/game-hand/game-hand.component';
import { GameGridComponent } from './components/game/game-grid/game-grid.component';
import { GameGridSpaceComponent } from './components/game/game-grid/game-grid-space/game-grid-space.component';
import { CardComponent } from './components/card/card.component';
import { OrbComponent } from './components/card/orb/orb.component';
import { CardgenComponent } from './components/cardgen/cardgen.component';
import { GameComponent } from './components/game/game.component';
import { CardgenService } from './services/cardgen.service';
import { CardMiniComponent } from './components/card-mini/card-mini.component';
import { GamestateService } from './services/gamestate.service';
import { ProgressBarComponent } from './components/card/progress-bar/progress-bar.component';
import { ActiveStatColorPipe } from './components/card/active-stat-color/active-stat-color.pipe';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { GameSidePanelComponent } from './components/game/game-side-panel/game-side-panel.component';
import { GameSidePanelAvatarComponent } from './components/game/game-side-panel/game-side-panel-avatar/game-side-panel-avatar.component';
import { MatchmakeComponent } from './components/matchmake/matchmake.component';


@NgModule({
  declarations: [
    AppComponent,
    CardComponent,
    OrbComponent,
    GameComponent,
    PropertiesPipe,
    CardgenComponent,
    GameGridComponent,
    GameGridSpaceComponent,
    GameHandComponent,
    CardMiniComponent,
    ProgressBarComponent,
    ActiveStatColorPipe,
    GameSidePanelComponent,
    GameSidePanelAvatarComponent,
    MatchmakeComponent
  ],
  imports: [
    BrowserModule,
    FlexLayoutModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    DragDropModule,
  ],
  providers: [
    CardgenService,
    HttpClient,
    GamestateService,
    ActiveStatColorPipe,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
