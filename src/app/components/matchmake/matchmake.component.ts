import { Component, OnInit } from '@angular/core';
import { PvpService, PvpRole } from '@app/services/pvp.service';
import { PvpEventHandlerService } from '@app/services/pvp-event-handler.service';

@Component({
  selector: 'app-matchmake',
  templateUrl: './matchmake.component.html',
  styleUrls: ['./matchmake.component.css']
})
export class MatchmakeComponent implements OnInit {

  public PvpRole = PvpRole;

  constructor(
    public pvp: PvpService,
    public pvpEventHandlerService: PvpEventHandlerService,
  ) { }

  ngOnInit(): void {
  }

}
