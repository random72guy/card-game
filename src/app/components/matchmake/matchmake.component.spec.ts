import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MatchmakeComponent } from './matchmake.component';

describe('MatchmakeComponent', () => {
  let component: MatchmakeComponent;
  let fixture: ComponentFixture<MatchmakeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MatchmakeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MatchmakeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
