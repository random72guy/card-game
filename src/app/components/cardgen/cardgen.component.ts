import { Component, OnInit } from '@angular/core';
import * as _ from 'lodash';
import { Minion } from '../../types/card/minion';
import { CardgenService } from '../../services/cardgen.service';
import { Util } from '../../util';

@Component({
  selector: 'app-cardgen',
  templateUrl: './cardgen.component.html',
  styleUrls: ['./cardgen.component.css']
})
export class CardgenComponent implements OnInit {

  card: Minion;

  constructor(
    private cardgen: CardgenService,
  ) { }

  ngOnInit() {
    this.cardgen.load().then(() => {

      this.card = new Minion();

      this.generate();

    });
  }

  generate() {
    this.card = this.cardgen.getRandomCard();
  }

}
