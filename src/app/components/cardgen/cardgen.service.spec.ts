import { TestBed, inject } from '@angular/core/testing';

import { CardgenService } from './cardgen.service';

describe('CardgenService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CardgenService]
    });
  });

  it('should be created', inject([CardgenService], (service: CardgenService) => {
    expect(service).toBeTruthy();
  }));
});
