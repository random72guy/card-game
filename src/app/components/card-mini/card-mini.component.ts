import { Component, OnInit, Input } from '@angular/core';
import { Body } from '@app/types/body';
import { Avatar } from '@app/types/avatar';
import { ActiveStatColorPipe } from '../card/active-stat-color/active-stat-color.pipe';

@Component({
  selector: 'app-card-mini',
  templateUrl: './card-mini.component.html',
  styleUrls: ['./card-mini.component.css']
})
export class CardMiniComponent implements OnInit {

  @Input() body: Body | Avatar;

  public borderColor: string; 

  constructor(
    private activeStatColorPipe: ActiveStatColorPipe,
  ) { }

  ngOnInit() {
    if (this.body instanceof Avatar) {
      this.borderColor = this.body.color;
    } else {
      this.borderColor = this.body.avatar.color;
    }
  }

  onAbilitiesClick() {
    alert('Coming soon: show abilities to play.');
  }

}
