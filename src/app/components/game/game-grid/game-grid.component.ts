import { Component, OnInit, Input } from '@angular/core';
import * as _ from 'lodash';
import { Space } from '../../../types/space';
import { Avatar } from '@app/types/avatar';
import { Util } from '@app/util';
import { Body } from '@app/types/body';
import { CardgenService } from '@app/services/cardgen.service';
import { GamestateService } from '@app/services/gamestate.service';

@Component({
  selector: 'app-game-grid',
  templateUrl: './game-grid.component.html',
  styleUrls: ['./game-grid.component.css']
})
export class GameGridComponent implements OnInit {

  @Input() public avatars: Avatar[];
  @Input() public sizeX: number;
  @Input() public sizeY: number;
  public spaces: Space[][];

  constructor(
    private cardgen: CardgenService,
    private gamestateService: GamestateService,
  ) { }

  ngOnInit() {
    this.spaces = _.times(this.sizeY, y => _.times(this.sizeX, x => new Space({ y, x })));

    _.forEach(this.avatars, avatar => this.placeBodyRandomly(avatar));

    this.gamestateService.registerGamestateElements({
      spaces: this.spaces,
      avatars: this.avatars,
    });

  }

  /**
   * Randomly places the Body on the game board. Avoids placement on an occupied space.
   * @param body A Body to place.
   */
  private placeBodyRandomly(body: Body) {
    const x = Util.randBetween(0, this.sizeX - 1);
    const y = Util.randBetween(0, this.sizeY - 1);
    const randSpace = this.spaces[x][y];
    if (!randSpace.body) randSpace.body = body;
    else this.placeBodyRandomly(body);
  }

  /**
   * A debug method to fill the board with cards. 
   */
  private debugFillBoard() {
    this.cardgen.load().then(() => {
      _.times((this.sizeY * this.sizeX) - this.avatars.length, () =>
        this.placeBodyRandomly(this.cardgen.getRandomCard()));
    });
  }

}
