import { Component, OnInit, Input } from '@angular/core';
import { Space } from '@app/types/space';
import { Body } from '@app/types/body';
import { CdkDragDrop, CdkDrag } from '@angular/cdk/drag-drop';
import { Util } from '@app/util';
import * as _ from 'lodash';
import { PvpEventService } from '@app/services/pvp-event.service';
import { PvpService, PvpRole } from '@app/services/pvp.service';

@Component({
  selector: 'app-game-grid-space',
  templateUrl: './game-grid-space.component.html',
  styleUrls: ['./game-grid-space.component.css']
})
export class GameGridSpaceComponent implements OnInit {

  @Input() public space: Space;

  constructor(
    private pvpEventService: PvpEventService,
    private pvpService: PvpService,
  ) { }

  ngOnInit() {
  }

  /**
   * Fires when something is dropped onto this space. 
   * @param event The event triggered by the receiving space.
   */
  public onDrop(event: CdkDragDrop<Body>) {

    const body: Body = event.item.data;
    const fromPosition = body.space ? body.space.position : null; 

    if(this.pvpService.role == PvpRole.CLIENT) this.pvpEventService.moveCard(body, fromPosition, this.space.position);

    if (body.canAssault()) {
      body.assault(this.space.body);

      // If the space is empty, move body there (if it survived a clash). 
      // Note that this logic's placement means bodies cannot move without being able to assault.
      if (!this.space.body && body.health.current > 0) this.space.body = body;
    }

  }



  /**
   * Returns true if an item can be placed here. Returns true if item is coming from an adjacent space, or if coming from the hand to an unoccupied space adjacent to the avatar.
   * @param event The CDK `cdkDropListEnterPredicate event. Thrown by the space the item is being dragged over.
   */
  public dropPredicate = (event: CdkDrag<Body>): boolean => {

    const body = event.data;

    // If coming from hand.
    if (!body.space) {
      return !this.space.body &&
        body.avatar.charge.ready &&
        Util.checkPositionsAreAdjacent(body.avatar.space.position, this.space.position);
    }

    // If coming from another space.
    const isValid = Util.checkPositionsAreAdjacent(this.space.position, body.space.position);
    return isValid;
  }

}
