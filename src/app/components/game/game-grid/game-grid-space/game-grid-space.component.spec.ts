import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GameGridSpaceComponent } from './game-grid-space.component';

describe('GameGridSpaceComponent', () => {
  let component: GameGridSpaceComponent;
  let fixture: ComponentFixture<GameGridSpaceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GameGridSpaceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GameGridSpaceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
