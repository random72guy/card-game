import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GameHandComponent } from './game-hand.component';

describe('GameHandComponent', () => {
  let component: GameHandComponent;
  let fixture: ComponentFixture<GameHandComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GameHandComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GameHandComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
