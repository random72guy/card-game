import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { Minion } from '../../../types/card/minion';
import { CardgenService } from '../../../services/cardgen.service';
import * as _ from 'lodash';

import { Subscription } from 'rxjs';
import { Charge } from '@app/types/charge';
import { CdkDragDrop } from '@angular/cdk/drag-drop';
import { Avatar } from '@app/types/avatar';

@Component({
  selector: 'app-game-hand',
  templateUrl: './game-hand.component.html',
  styleUrls: ['./game-hand.component.css']
})
export class GameHandComponent implements OnInit, OnDestroy {

  @Input() public avatar: Avatar;
  @Input() public hideCards: boolean = false;

  private subscriptions: { [name: string]: Subscription } = {};
  public drawCharge = new Charge(2);
  public maxSize: number = 5;
  public hand: Minion[] = [];  // TODO: Support other card types.
  public shadowStr: string;
  public borderColor: string;

  constructor(
    private cardGen: CardgenService,
  ) {

  }

  ngOnInit() {
    this.cardGen.load().then(() => {
      _.times(this.maxSize, () => this.draw());
    });

    // Start draw timer:
    this.subscriptions.drawTimer = this.drawCharge.onCharge.subscribe(() => {
      this.draw();
      this.drawCharge.reset();
    });

  }

  ngOnDestroy() {
    _.forEach(this.subscriptions, sub => sub.unsubscribe());
  }

  private draw() {
    if (this.avatar.hand.length < this.maxSize) {
      const card = this.cardGen.getRandomCard();
      card.avatar = this.avatar;
      this.avatar.hand.push(card);
    }
  }

  public onCardDrop(card: Minion, event: CdkDragDrop<Body>) {

    if (event.container == event.previousContainer) return;

    _.remove(this.avatar.hand, card);

    this.avatar.health.decrease(card.cost);
    this.avatar.charge.deplete();
  }

}
