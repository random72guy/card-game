import { Component, OnInit } from '@angular/core';
import * as _ from 'lodash';
import { Avatar } from '@app/types/avatar';
import { GamestateService } from '@app/services/gamestate.service';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.css']
})
export class GameComponent implements OnInit {

  numPlayers: number = 2;
  width: number = 7;
  height: number = 7;
  avatars: Avatar[] = [];
  gameOver: boolean = false;

  private avatarColors = ['maroon', 'pink', 'lightskyblue', 'magenta',];

  constructor(
    private gamestateService: GamestateService,
  ) { }

  ngOnInit() {
    _.times(this.numPlayers, i => this.createAvatar(`Player ${i + 1}`));

    this.gamestateService.gamestate.$gameOver.subscribe(() => this.endGame());
  }

  private createAvatar(name: string) {
    const avatar = new Avatar();
    avatar.name = name;
    avatar.color = this.avatarColors.pop();
    avatar.health.start = 30;
    this.avatars.push(avatar);
  }

  private endGame() {
    this.gameOver = true;
    console.log('ENDING GAME!!!');
  }


}
