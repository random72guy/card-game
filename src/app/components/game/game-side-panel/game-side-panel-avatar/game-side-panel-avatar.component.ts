import { Component, OnInit, Input } from '@angular/core';
import { Avatar } from '@app/types/avatar';

@Component({
  selector: 'app-game-side-panel-avatar',
  templateUrl: './game-side-panel-avatar.component.html',
  styleUrls: ['./game-side-panel-avatar.component.css']
})
export class GameSidePanelAvatarComponent implements OnInit {

  @Input() avatar: Avatar;
  @Input() label: String;
  @Input() imagePath: string;

  constructor() { }

  ngOnInit(): void {
  }

}
