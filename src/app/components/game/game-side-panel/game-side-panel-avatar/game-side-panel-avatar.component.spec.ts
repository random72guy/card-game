import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GameSidePanelAvatarComponent } from './game-side-panel-avatar.component';

describe('GameSidePanelAvatarComponent', () => {
  let component: GameSidePanelAvatarComponent;
  let fixture: ComponentFixture<GameSidePanelAvatarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GameSidePanelAvatarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GameSidePanelAvatarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
