import { Component, OnInit, Input } from '@angular/core';
import { Avatar } from '@app/types/avatar';

@Component({
  selector: 'app-game-side-panel',
  templateUrl: './game-side-panel.component.html',
  styleUrls: ['./game-side-panel.component.css']
})
export class GameSidePanelComponent implements OnInit {

  @Input() avatars: Avatar[];

  constructor() { }

  ngOnInit(): void {
  }

}
