import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GameSidePanelComponent } from './game-side-panel.component';

describe('GameSidePanelComponent', () => {
  let component: GameSidePanelComponent;
  let fixture: ComponentFixture<GameSidePanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GameSidePanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GameSidePanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
