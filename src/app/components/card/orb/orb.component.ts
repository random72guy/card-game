import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-orb',
  templateUrl: './orb.component.html',
  styleUrls: ['./orb.component.css']
})
export class OrbComponent implements OnInit {

  @Input() val: string | number;
  @Input() color: string;

  constructor() { }

  ngOnInit() {
  }

}
