import { Component, OnInit, Input } from '@angular/core';
import * as _ from 'lodash';
import { Minion } from '../../types/card/minion';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit {

  @Input() card: Minion;
  @Input() showLore = true;
  backgroundColorStr: string;
  shadowStr: string;

  constructor() { }

  ngOnInit() {
    if (!this.card) {
      console.error('Card component invoked without a card.');
      return;
    }
    this.backgroundColorStr = 'linear-gradient(' + _.map(this.card.affinities, 'color').toString() + ')';
    this.setBackground();
  }

  private setBackground() {
    this.shadowStr = `0 0 ${this.card.potential * 5}px ${Math.pow(this.card.potential * .5, 2)}px ${this.card.avatar.color}`;
  }

}
