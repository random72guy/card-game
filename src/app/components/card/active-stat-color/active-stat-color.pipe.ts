import { Pipe, PipeTransform } from '@angular/core';
import { Body } from '@app/types/body';
import { Mode } from '@app/types/card/mode';

@Pipe({
  name: 'activeStatColor',
  pure: false,
})
export class ActiveStatColorPipe implements PipeTransform {

  transform(b: Body, ...args: unknown[]): string {
    if (b.mode === Mode.ASSAULT) return 'red';
    if (b.mode === Mode.GUARD) return 'green';
  }

}
