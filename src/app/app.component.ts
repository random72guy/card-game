import { Component } from '@angular/core';
import { UIRouter  } from '@uirouter/angular';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Rogue';

  constructor(
    public router: UIRouter,
  ) {
    
  }
}
