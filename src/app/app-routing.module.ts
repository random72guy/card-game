import { NgModule } from '@angular/core';
import { RootModule, UIRouterModule } from '@uirouter/angular';
import { CardComponent } from './components/card/card.component';
import { GameComponent } from './components/game/game.component';
import { CardgenComponent } from './components/cardgen/cardgen.component';

const rootModule: RootModule = {
  states: [
    {
      name: "Game",
      url: "/game",
      component: GameComponent,
    },
    {
      name: "Card Generator",
      url: "/cardgen",
      component: CardgenComponent,
    },
  ],
  useHash: true
};

@NgModule({
  imports: [UIRouterModule.forRoot(rootModule)],
  exports: [UIRouterModule]
})
export class AppRoutingModule { }
