import * as _ from 'lodash';
import { GridPosition } from './types/grid-position';

export class Util {
    static randBetween(min, max): number {
        return Math.floor(Math.random() * (max - min + 1) + min);
    }

    /**
     * Returns false if a given argument is undefined or null.
     * 
     * @example  
     * ```
     * if (!Util.validateRequiredArguments({myArg1, myArg2})) return;
     * ```
     * 
     * @param args Values to check for. Key/value pairs.
     * 
     */
    static validateRequiredArguments(args: { [arg: string]: any }): boolean {
        return !_.some(args, (val, key) => {
            const invalid = _.isNil(val);
            if (invalid) console.error(`Missing required argument: ${key}. Value received:`, val);
            return invalid;
        });
    }

    static checkPositionsAreAdjacent(target: GridPosition, source: GridPosition): boolean {
        const isValidX = Math.abs(source.x - target.x) <= 1;
        const isValidY = Math.abs(source.y - target.y) <= 1;
        return isValidX && isValidY;
    }
}
