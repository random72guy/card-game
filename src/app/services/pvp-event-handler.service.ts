import { Injectable } from '@angular/core';
import { PeerMessage, PeerMessageType } from '@app/types/peerMessage';
import Peer from 'peerjs';
import { parse, stringify } from 'flatted/esm';
import { Gamestate } from '@app/types/gamestate';
import { interval, from, Subscription } from 'rxjs';
import { GamestateService } from './gamestate.service';
import { Card } from '@app/types/card';
import { Body } from '@app/types/body';
import { Minion } from '@app/types/card/minion';
import { GridPosition } from '@app/types/grid-position';
import * as _ from 'lodash';
import { Avatar } from '@app/types/avatar';

@Injectable({
  providedIn: 'root'
})
export class PvpEventHandlerService {

  public conn: Peer.DataConnection;
  private syncTimer: Subscription;

  constructor(
    private gamestateService: GamestateService,
  ) { }

  public onConnect(conn: Peer.DataConnection) {
    // Periodically send state.
    this.conn = conn;
    this.sendGamestate();
    if (!this.syncTimer) this.syncTimer = interval(5000).subscribe(val => {
      this.sendGamestate();
    });
  }

  public handleMessage(message: PeerMessage) {
    this.handlers[message.type](message.body);
  }

  private sendMessage(message: PeerMessage) {
    this.conn.send(stringify(message));
  }

  public sendGamestate() {
    this.sendMessage({
      type: PeerMessageType.GAMESTATE,
      body: this.gamestateService.gamestate,
    });
  }

  handlers: { [messageType in PeerMessageType]: PeerMessageHandler } = {
    GAMESTATE: (messageBody) => { },
    MOVE_CARD: (messageBody) => {
      const card: Body = new Body(messageBody.card).sync(messageBody.card, this.getAvatarForIncomingBody(messageBody.card));
      const fromPosition: GridPosition = messageBody.fromPosition;
      const toPosition: GridPosition = messageBody.toPosition;

      console.log('Moving card to position:', { card, fromPosition, toPosition });

      this.gamestateService.moveCard(card, fromPosition, toPosition);
    },
  };

  public getAvatarForIncomingBody(incoming: Body): Avatar {
    return _.find(this.gamestateService.gamestate.avatars, avatar => avatar.id == incoming.avatar.id);
  }


}

type PeerMessageHandler = (body: any) => void;