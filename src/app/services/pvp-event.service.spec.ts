import { TestBed } from '@angular/core/testing';

import { PvpEventService } from './pvp-event.service';

describe('PvpEventService', () => {
  let service: PvpEventService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PvpEventService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
