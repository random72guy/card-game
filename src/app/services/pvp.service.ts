import { Injectable } from '@angular/core';
import Peer from 'peerjs';
import { Gamestate } from '@app/types/gamestate';
import { PeerMessage, PeerMessageType } from '@app/types/peerMessage';
import * as _ from 'lodash';
import { interval } from 'rxjs';
import { parse, stringify } from 'flatted/esm';
import { Minion } from '@app/types/card/minion';
import { Avatar } from '@app/types/avatar';
import { PvpEventService } from './pvp-event.service';
import { PvpEventHandlerService } from './pvp-event-handler.service';

@Injectable({
  providedIn: 'root'
})
export class PvpService {

  public peerIdInput: string = '';
  private peer: Peer;
  private peerIdNamespacePrefix = 'battle-city-cardgame-';
  public roomKey: string;
  public conn: Peer.DataConnection;
  public status: 'Connected' | 'Preparing to Host' | 'Hosting' | 'Connecting' | 'Failure';
  public role: PvpRole;

  constructor(
    private pvpEventService: PvpEventService,
    private pvpEventHandlerService: PvpEventHandlerService,
  ) { }

  private onConnection(conn: Peer.DataConnection, role: PvpRole) {
    this.conn = conn;
    console.log('Connected!', conn);
    this.status = 'Connected';
    this.role = role;

    // Initialize correct event service.
    if (this.role == PvpRole.CLIENT) this.pvpEventService.onConnect(conn);
    else this.pvpEventHandlerService.onConnect(conn);

    // Subscribe to incoming data, and route responses to correct handler.
    this.conn.on('data', (data: string) => {
      const message: PeerMessage = parse(data);
      console.log('Received data from conn:', message);
      if (this.role == PvpRole.CLIENT) this.pvpEventService.handleMessage(message);
      else this.pvpEventHandlerService.handleMessage(message);
    });

  }

  public host() {
    this.status = 'Preparing to Host';

    this.generateRoomKey();
    this.peer = new Peer(this.peerIdNamespacePrefix + this.roomKey, {
      debug: 2,
    });

    this.peer.on('open', id => {
      console.log('Hosting. Peer ID is:', id);
      this.status = 'Hosting';
    });
    this.peer.on('connection', (conn) => {
      console.log('Client connected.');
      this.onConnection(conn, PvpRole.HOST);
    });

  }

  public connectAsClient() {

    const hostId = this.peerIdNamespacePrefix + this.roomKey;
    console.log('Connecting to host:', hostId);

    const peer = new Peer({ debug: 2 });
    peer.on('error', err => {
      this.status = 'Failure';
    });

    const conn = peer.connect(hostId);
    this.onConnection(conn, PvpRole.CLIENT);
  }

  // TODO: Remove this.
  public debugSendTestMessage() {
    console.log('Sending test message...');
    this.conn.send('Test send message.');
  }

  private generateRoomKey(): void {
    this.roomKey = _.random(1000, 9999).toString();
  }

}

export enum PvpRole {
  CLIENT = 'CLIENT',
  HOST = 'HOST',
}
