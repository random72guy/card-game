import { TestBed } from '@angular/core/testing';

import { PvpEventHandlerService } from './pvp-event-handler.service';

describe('PvpEventHandlerService', () => {
  let service: PvpEventHandlerService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PvpEventHandlerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
