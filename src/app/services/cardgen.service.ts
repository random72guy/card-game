import { Injectable } from '@angular/core';
import * as Papa from 'papaparse';
import * as _ from 'lodash';
import { HttpClient } from '@angular/common/http';
import * as rwc from 'random-weighted-choice';
import { Affinity } from '../types/card/affinity';
import { Modifier } from '../types/card/modifier';
import { Role } from '../types/card/role';
import { Species } from '../types/card/species';
import { Tribe } from '../types/card/tribe';
import { Ability } from '../types/card/ability';
import { Minion } from '../types/card/minion';
import { Util } from '../util';
import { AbilityActivationType } from '@app/types/card/ability-activation-type';
import { Health } from '@app/types/health';
import { Charge } from '@app/types/charge';
import { Mode } from '@app/types/card/mode';


@Injectable()
export class CardgenService {

  affinities: Affinity[];
  modifiers: Modifier[];
  roles: Role[];
  species: Species[];
  tribes: Tribe[]; // Tribes have lore.
  abilities: Ability[];

  parseResults: { [fileName: string]: any } = {
    affinities: undefined,
    modifiers: undefined,
    roles: undefined,
    species: undefined,
    tribes: undefined,
    lore: undefined,
    abilities: undefined,
    abilityMultipliers: undefined,
  };

  constructor(
    private http: HttpClient,
  ) { }

  public load(): Promise<any> {

    // Load text for each file.
    let fileNames = _.keys(this.parseResults);
    let promiseMap = _.map(fileNames, fileName => {
      return {
        fileName: fileName,
        promise: this.http.get('./assets/' + fileName + '.csv', { responseType: 'text' }).toPromise()
      }
    });
    return Promise.all(_.map(promiseMap, 'promise')).then(responses => {
      _.forEach(responses, (response, i) => {
        let fileName = promiseMap[i].fileName;
        this.parseResults[fileName] = Papa.parse(response, {
          header: true
        }).data;
      });

      // Load Affinities
      this.affinities = _.map(this.parseResults.affinities, (res: any) => {
        return new Affinity({
          category: res.Category,
          name: res.Name,
          synonyms: [],
          color: res.Color,
          advantageVs: res.AdvantageVs,
          weaknessVs: res.WeaknessVs,
        });
      });

      // Load Tribes
      this.tribes = _.map(this.parseResults.tribes, (res: any) => {
        return new Tribe({
          name: res.Tribe,
          species: res.IncludedSpecies,
          lore: _.chain(this.parseResults.lore).filter({ Tribe: res.Tribe }).map('Lore').valueOf(),
        });
      });

      // Load Species
      this.species = _.map(this.parseResults.species, (res: any) => {
        return new Species({
          category: res.Category,
          name: res.Name,
          affinities: res['Required Affinities'],
          modifiers: res['Required Modifiers'],
        });
      });

      // Load Role
      this.roles = _.map(this.parseResults.roles, (res: any) => {
        return new Role({
          name: res.Name
        });
      });

      // Load abilities
      this.abilities = _.map(this.parseResults.abilities, (res: any) => {
        let multipliers = _.chain(this.parseResults.abilityMultipliers)
          .filter({ Ability: res.Name })
          .map(item => {
            return {
              thingName: <string>item['Affinity, Species, Tribe, Role'],
              multiplier: parseInt(item.Multiplier) * .01, // Adjust for percentage
            }
          })
          .value();
        return new Ability({
          name: res.Name,
          description: res.Description,
          baseRarityMultiplier: parseInt(res['Base Rarity Multiplier']) * .01, // Adjust for percentage
          multipliers: multipliers,
          activationTypes: [AbilityActivationType.ACTIVATED],
          level: 1,
          method: '',
        });
      });


      return Promise.resolve(this);

    });

  }

  private getRandomAbilities(card: Minion): Ability[] {

    var abilities: Ability[] = [];
    let numAbilities = Math.round(card.potential / 3);
    let cardThingNames: string[] = _.flatten([
      _.map(card.affinities, 'name'),
      card.species.name,
      card.tribe.name,
      card.role.name,
    ]);

    let weightedItems: { weight: number, id: string }[] = _.map(this.abilities, ability => {
      let applicableMultipliers: number[] = _.chain(ability.multipliers).filter(multiplier => {
        return _.includes(cardThingNames, multiplier.thingName);
      }).map('multiplier').value();
      var ret = {
        id: ability.name,
        weight: ability.baseRarityMultiplier,
      };
      if (applicableMultipliers.length) ret.weight = ret.weight * applicableMultipliers.reduce(_.multiply); // Factor in any additional modifiers.
      return ret;
    });

    while (abilities.length < numAbilities) {
      let abilityName = rwc(weightedItems);
      abilities.push(_.find(this.abilities, ability => ability.name == abilityName));
    }

    return abilities;
  }

  public getRandomCard(): Minion {
    const card = new Minion();

    const imgNames = [
      'Angel',
      'Centaur',
      'Demon',
      'Dragon',
      'Elf',
      'Pegasus',
      'Spirit',
    ];
    const imgName = _.sample(imgNames);

    card.affinities = [_.sample(this.affinities), _.sample(this.affinities)];
    card.offence = Util.randBetween(0, 9);
    card.defence = Util.randBetween(1, 9);
    card.imgUrl = `assets/card-art/cartoon/${imgName}.png`;
    card.tribe = _.sample(this.tribes);
    card.species = _.sample(this.species);
    card.role = _.sample(this.roles);
    card.speed = Util.randBetween(1, 9);
    card.potential = Util.randBetween(1, 9);
    card.abilities = this.getRandomAbilities(card);
    card.lore = _.sample(card.tribe.lore);
    card.health = new Health(Util.randBetween(1,9));
    card.mode = Mode.ASSAULT;
    card.cost = Util.randBetween(1,9);
    card.init();

    return card;
  }

}
