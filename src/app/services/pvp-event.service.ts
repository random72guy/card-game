import { Injectable } from '@angular/core';
import { PeerMessage, PeerMessageType } from '@app/types/peerMessage';
import Peer from 'peerjs';
import { Gamestate } from '@app/types/gamestate';
import { GamestateService } from './gamestate.service';
import { GridPosition } from '@app/types/grid-position';
import { parse, stringify } from 'flatted/esm';
import { Body } from '@app/types/body';

@Injectable({
  providedIn: 'root'
})
export class PvpEventService {

  public conn: Peer.DataConnection;

  constructor(
    private gamestateService: GamestateService,
  ) { }

  public onConnect(conn: Peer.DataConnection) {
    this.conn = conn;
  }

  private sendMessage(message: PeerMessage) {
    this.conn.send(stringify(message));
  }

  public handleMessage(message: PeerMessage) {
    if (message.type == PeerMessageType.GAMESTATE) this.syncGamestate(message.body as Gamestate);
  }

  public syncGamestate(gamestate: Gamestate) {
    this.gamestateService.gamestate.sync(gamestate);
  }

  /**
   * Moves a card from the hand to the space indicated. 
   * @param card A card that has been moved to the board.
   */
  public moveCard(card: Body, fromPosition: GridPosition, toPosition: GridPosition) {
    this.sendMessage({
      type: PeerMessageType.MOVE_CARD,
      body: { card, fromPosition, toPosition }
    });
  }

}
