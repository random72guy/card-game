import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Gamestate } from '@app/types/gamestate';
import * as _ from 'lodash';
import { GridPosition } from '@app/types/grid-position';
import { Space } from '@app/types/space';
import { Body } from '@app/types/body';

@Injectable({
  providedIn: 'root'
})
export class GamestateService {

  private _gamestate: Gamestate = new Gamestate();

  public events = {
    summon: new Subject<null>(),
  };

  constructor() { }

  /**
  * Registers objects to synchronize across clients.
  * @param elements Pointers to objects that should be synchronized across clients.
  */
  public registerGamestateElements(elements: Partial<Gamestate>) {
    _.assign(this._gamestate, elements);
    if (this.gamestate.avatars) this.gamestate.watchForGameEnd();
  }


  public get gamestate() {
    return this._gamestate;
  }

  public moveCard(card: Body, fromPosition: GridPosition, toPosition: GridPosition) {
    const fromSpace = this.getSpaceForPosition(fromPosition);
    const toSpace = this.getSpaceForPosition(toPosition);

    if (fromSpace) fromSpace.body = null;
    else this.removeCardFromHand(card);

    toSpace.body = card;
    
  }

  public getSpaceForPosition(position: GridPosition): Space {
    if (!position) return null;
    return this.gamestate.spaces[position.y][position.x];
  }

  public removeCardFromHand(card: Body) {
    const hand = card.avatar.hand;
    const res = _.remove(hand, c => card.id == c.id);
    console.log('removeCardFromHand', {res, hand}, card.avatar);
  }

}
