import { Body, BODY_SUBTYPE } from './body';
import * as _ from 'lodash';
import { Health } from './health';
import { Minion } from './card/minion';

export class Avatar extends Body {

    color: string;
    hand: Minion[] = [];

    constructor(avatar?: Avatar) {
        super(avatar);
        this.bodySubtype = BODY_SUBTYPE.AVATAR;
        _.defaults(this, avatar);

        this.imgUrl = `assets/card-art/cartoon/Wizard.png`;
        this.speed = 5;
        this.health = new Health(10);
        this.avatar = this;

        this.init();
    }

    init() {
        super.init();
    }

    sync(incoming: Avatar): this {
        delete incoming.space; // Prevent recursive sync errors.

        _.merge(this, incoming);
        this.hand = this.hand.map(card => new Minion(card));

        return this;
    }

}