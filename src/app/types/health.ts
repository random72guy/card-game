import { Observable, Subject } from 'rxjs';
import * as _ from 'lodash';

export class Health {
    private _start: number;
    private _current: number;

    public current$: Subject<number> = new Subject<number>();

    constructor(startHealth: number) {
        this.start = startHealth;
    }

    increase(amt: number) {
        if (amt < 0) return;
        this.current = Math.min(this.start, this.current + amt);
    }

    decrease(amt: number){
        if (amt < 0) return;
        this.current = Math.max(0, this.current - amt);
    }

    get percent(): number {
        return Math.round(this._current / this._start * 100) ;
    }
    
    get start(): number {
        return this._start;
    }

    /**
     * Sets the starting/max health and reinitializes current to equal the starting health.
     */
    set start(newVal: number) {
        this._start = newVal;
        this._current = this._start;
    }

    /**
     * Sets `_current` and fires observable.
     */
    set current(newVal: number) {
        this._current = newVal;
        this.current$.next(this._current);
    }

    get current() {
        return this._current;
    }

    public static from(incoming: Health): Health {
        const newHealth = new Health(incoming.start);
        _.merge(newHealth, incoming);
        return newHealth;
    }


}