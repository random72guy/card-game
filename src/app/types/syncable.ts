export interface Syncable<T> {

    /**
     * Updates this instance's properties based on an incoming uninitialized object / data model. May recursively call sync methods. Sometimes creating a new instance will need to be created and then sync'd. 
     * 
     * @param incoming A new static object to use to populate this object. 
     */
    sync(incoming: T): void;
}