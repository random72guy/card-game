import * as _ from 'lodash';
import { Card } from './card';
import { Mode } from './card/mode';
import { Ability } from './card/ability';
import { Health } from './health';
import { Charge } from './charge';
import { Space } from './space';
import { Avatar } from './avatar';
import { Syncable } from './syncable';

export class Body extends Card {

    static id: number = 0;

    id: number;
    name: string;
    offence: number;
    defence: number;
    abilities: Ability[];
    lore: string;
    mode: Mode;
    health: Health;
    charge: Charge;
    bodySubtype?: BODY_SUBTYPE;

    space: Space;
    avatar: Avatar;

    constructor(body?: Body) {
        super(body);

        _.defaults(this, body);

        if (!this.id) this.id = ++Body.id;

        this.init();
    }

    public move(space: Space) {
        this.space = space;
        this.charge.deplete();
    }

    init() {
        super.init();
        if (this.speed) this.charge = new Charge(this.speed);
    }

    toggleActiveMode() {
        if (!this.mode || !this.charge.ready) return; // Handle Avatars, which may have no stats/modes.
        if (this.mode === Mode.ASSAULT) this.mode = Mode.GUARD;
        else this.mode = Mode.ASSAULT;
        this.charge.deplete();
    }

    assault(target: Body) {
        if (target && this.canAssault()) {
            this.clash(this, target);
            this.clash(target, this);

            this.charge.deplete();
            target.charge.deplete();

        }
    }

    private clash(initiating: Body, receiving: Body) {
        const activeOffence = initiating.mode == Mode.ASSAULT ? initiating.offence * 2 : initiating.offence;
        const activeDefense = Math.min(receiving.mode == Mode.GUARD ? receiving.defence * 2 : receiving.defence, 9);
        const dmgMultiplier = 1- (activeDefense / 10);
        const dmg = Math.max(Math.round(activeOffence * dmgMultiplier), 1);

        console.log(`${receiving.name} (${receiving.mode}) receiving ${dmg}/${receiving.health.current} damage.`, {activeOffence, activeDefense, dmgMultiplier, dmg});

        receiving.health.decrease(dmg);
    }

    public canAssault(): boolean {
        return this.charge.ready && this.mode !== Mode.GUARD;
    }

    public sync(incoming: Body, avatar: Avatar): this {

        _.merge(this, incoming);
        this.health = Health.from(incoming.health);
        this.charge = Charge.from(incoming.charge, incoming.speed);
        this.avatar = avatar;

        return this;
    }

}

export enum BODY_SUBTYPE {
    AVATAR = 'AVATAR',
    MINION = 'MINION',
}
