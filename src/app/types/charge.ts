import * as _ from 'lodash';
import { interval, Subject } from 'rxjs';
import { Util } from '@app/util';

export class Charge {

    private cur: number = 0;
    private max: number = 100;
    private speed: number;

    public onCharge: Subject<void> = new Subject();

    constructor(speed: number) {
        if (!Util.validateRequiredArguments({speed})) return;

        this.cur = 0;
        this.speed = speed;

        interval(100).subscribe(val => {
            this.chargeUp();
        });

    }

    /**
     * Increments `cur` by `speed` until it reaches `max`, and emits the `onCharge` event on reaching `max`.
     */
    private chargeUp() {
        if (this.cur <= this.max) {
            this.cur = Math.min(this.max, this.cur + this.speed);
            if (this.cur == this.max) this.onCharge.next();
        }
    }

    public deplete() {
        this.cur = 0;
    }

    public get percent(): number {
        const percent = Math.round( (this.cur / this.max) * 100);
        return percent;
    }

    public get ready(): boolean {
        return this.cur == this.max;
    }

    public reset() {
        this.cur = 0;
    }

    public fullyCharge() {
        this.cur = this.max;
    }

    public set(amt: number) {
        this.cur = Math.min(this.max, amt);
    }

    public static from(incoming: Charge, speed: number): Charge {
        const newCharge = new Charge(speed);
        _.merge(newCharge, incoming);
        return newCharge;
    }
}
