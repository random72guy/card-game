import * as _ from 'lodash';
import { Tribe } from './card/tribe';
import { Affinity } from './card/affinity';

export class Card {
  speed: number;
  imgUrl: string;
  tribe: Tribe;
  potential: number;
  affinities: Affinity[];
  cost: number;
  constructor(card?: Card) {
    _.defaults(this, card);
  }

  public get serializable(): Partial<this> {
    const ret = _.clone(this);
    return ret;
  }

  init() {
    
  }

}