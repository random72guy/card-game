import { Body } from './body';
import { GridPosition } from './grid-position';
import * as _ from 'lodash';
import { Subscription } from 'rxjs';
import { Syncable } from './syncable';
import { Avatar } from './avatar';

export class Space {
  private _body: Body;
  public isValidMoveTarget: boolean = false;

  private bodyHealth$: Subscription;


  constructor(
    public position: GridPosition,
  ) {

  }

  /**
   * Returns true if this space can be traveled to from the starting position. Essentially checks whether the square is within one square of the original.
   * 
   * @param fromPosition A position considering traveling to this location.
   */
  public checkValidMoveTarget(fromPosition: GridPosition): boolean {
    return _.inRange(this.position.x, fromPosition.x-1, fromPosition.x+2) &&
    _.inRange(this.position.x, fromPosition.y-1, fromPosition.y+2);
  }

  public updateValidMoveTarget(fromPosition: GridPosition) {
    this.isValidMoveTarget = this.checkValidMoveTarget(fromPosition);
  }


  /**
   * Sets or removes a body, and updates the health subscription. 
   * @param body The new body. If null, clears the existing body. 
   */
  public set body(body: Body) {
    this._body = body;
    
    if (!body) return;

    if (this._body.space) this._body.space.body = null; // Clear from former space.
    this._body.move(this);
    
    // Clear space body if the body's health goes to 0.
    if (this.bodyHealth$) this.bodyHealth$.unsubscribe();
    this.bodyHealth$ = body.health.current$.subscribe(current => {
      if (current <= 0) this._body = null;
    });
  }

  public get body() {
    return this._body;
  }

  public sync(incoming: Space, avatars: Avatar[]) {

    // TODO: Initialize object based on Body subclass.
    const baseIncomingBody: Body = incoming['_body'];
    if (baseIncomingBody) {

      const bodyAvatar = _.find(avatars, avatar => avatar.id == baseIncomingBody.avatar.id);
      const incomingBody: Body = new Body().sync(baseIncomingBody, bodyAvatar);

      if (!this.body) this.body = incomingBody;
      else this.body.sync(baseIncomingBody, bodyAvatar);
    } 
    else this.body = null;

  }

  public toString(): string {
    return `${this.position.x}, ${this.position.y}`;
  }

}
