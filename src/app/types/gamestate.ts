import { Space } from './space';
import * as _ from 'lodash';
import { Avatar } from './avatar';
import { Subject } from 'rxjs';

export class Gamestate {
    spaces: Space[][];
    avatars: Avatar[];
    $gameOver: Subject<void> = new Subject();

    constructor(gamestate?: Gamestate) {
        _.merge(this, gamestate);
    }

    public getAvatarById(avatarId: number): Avatar {
        return _.find(this.avatars, avatar => avatar.id == avatarId);
    }

    public sync(incoming: Gamestate) {

        console.log('Syncing gamestate: ', this, incoming);

        // Sync avatars
        this.avatars.forEach((avatar, i) => avatar.sync(incoming.avatars[i]));
        this.checkGameEnded()

        // Sync each space.
        this.spaces.forEach((row, x) => row.forEach((space, y) => {
            const incomingSpace = incoming.spaces[x][y];
            space.sync(incomingSpace, this.avatars);
        }));

    }

    public watchForGameEnd() {
        this.avatars.forEach(avatar => {
            avatar.health.current$.subscribe(val => {
                this.checkGameEnded();
            });
        })
    }

    private checkGameEnded() {
        if (this.isGameOver()) this.$gameOver.next();
    }

    /**
   * Returns `true` if the game is over.
   */
    private isGameOver(): boolean {
        // Check if 1 or fewer avatars are alive.
        return _.filter(this.avatars, avatar => avatar.health.current > 0).length <= 1;
    }

}