import { Gamestate } from './gamestate';

/**
 * Represents a message. Messages may have a type that corresponds with the body interface. 
 */
export interface PeerMessage {
    type: PeerMessageType;
    body: object; 
}

export enum PeerMessageType {
    GAMESTATE = 'GAMESTATE',
    MOVE_CARD = 'MOVE_CARD',
}