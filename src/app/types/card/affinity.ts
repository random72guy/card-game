export class Affinity {
    category: string;
    name: string;
    synonyms: string[];
    color: string;
    advantageVs: Affinity[];
    weaknessVs: Affinity[];

    constructor(obj: Affinity){
        this.category = obj.category;
        this.name = obj.name;
        this.color = obj.color;
        this.synonyms = obj.synonyms;
        this.advantageVs = obj.advantageVs;
        this.weaknessVs = obj.weaknessVs;
    }
}
