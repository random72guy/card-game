import { Affinity } from "./affinity";
import { Modifier } from "./modifier";

export class Species {
    category: string;
    name: string;
    affinities: Affinity[];
    modifiers: Modifier[];

    constructor(species: Species) {
        this.category = species.category;
        this.name = species.name;
        this.affinities = species.affinities;
        this.modifiers = species.modifiers;
    }
}
