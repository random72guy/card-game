import { Affinity } from "./affinity";
import { Tribe } from "./tribe";
import { Species } from "./species";
import { Role } from "./role";
import * as _ from 'lodash';
import { Body, BODY_SUBTYPE } from '../body';

export class Minion extends Body {

  species: Species;
  role: Role;
  bodySubtype: BODY_SUBTYPE.MINION;

  constructor(minion?: Minion) {
    super(minion);
    _.defaults(this, minion);
  }

  get name() {
    return `${this.tribe.name} ${this.species.name} ${this.role.name}`;
  }

  init() {
    super.init();
  }
}
