import { Species } from "./species";

export class Tribe {
    name: string;
    lore: string[];
    species: Species[];

    constructor(tribe: Tribe) {
        this.name = tribe.name;
        this.lore = tribe.lore;
        this.species = tribe.species;
    }
}
