export enum AbilityActivationType {
    ACTIVATED,
    STATIC,
    TRIGGERED,
}