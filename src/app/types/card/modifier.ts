import { Affinity } from "./affinity";

export class Modifier {
    name: string;
    synonyms: string[];
    advantageVs: Affinity[];
    weaknessVs: Affinity[];
    grantedAffinity: Affinity;
}
