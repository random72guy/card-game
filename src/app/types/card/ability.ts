import { AbilityMultiplier } from "./ability-multiplier";
import { AbilityActivationType } from './ability-activation-type';

export class Ability {
    name: string;
    description: string;
    baseRarityMultiplier: number;
    multipliers: AbilityMultiplier[];
    level: number = 0;
    method: string;
    activationTypes: AbilityActivationType[] = [];

    constructor(obj: Ability) {
        this.name = obj.name;
        this.description = obj.description;
        this.baseRarityMultiplier = obj.baseRarityMultiplier;
        this.multipliers = obj.multipliers;
    }
}
