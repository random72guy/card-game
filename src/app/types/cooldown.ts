import { interval ,  Subject } from 'rxjs';

export class Cooldown {

  private cur: number = 0;
  private max: number;

  public onCooldown: Subject<void> = new Subject();

  constructor(max: number, speed: number) {
    this.max = max;
    this.cur = max;

    interval(1000).subscribe(val => {
      if (this.rollDown()) this.onCooldown.next();
    });

  }

  /**
   * Decrements a number and rolls over to the max value if necessary. Returns `true` if it rolled over.
   */
  private rollDown(): boolean {
    if (this.cur > 0) this.cur--;
    else this.cur = this.max;
    return this.cur === this.max;
  }

  public get val() {
    return this.cur;
  }

  public reset() {
    this.cur = this.max;
  }

  public get percent() {
    return Math.round( (this.cur / this.max) * 100);
  }
}
